import { createAction, props } from '@ngrx/store';
import { WeightEntry } from '../models';

const PREFIX = '[Weight Entries | Collection Page]';

export const enter = createAction(`${PREFIX} Enter`);

export const remove = createAction(`${PREFIX} Remove`, props<{ weightEntry: WeightEntry }>());
