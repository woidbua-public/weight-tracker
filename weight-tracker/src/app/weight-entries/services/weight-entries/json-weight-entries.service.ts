import { Injectable } from '@angular/core';
import { Guid } from 'guid-typescript';
import { Observable, of } from 'rxjs';
import jsonData from '../../fixtures/weight-entries.fixture.json';
import { WeightEntry } from '../../models';
import { WeightEntriesService } from './weight-entries.service';

@Injectable()
export class JsonWeightEntriesService extends WeightEntriesService {
  private weightEntries: WeightEntry[];

  constructor() {
    super();
    this.weightEntries = jsonData.map<WeightEntry>((wed) => ({
      id: wed.id,
      date: new Date(wed.date),
      weight: wed.weight,
    }));
  }

  getAll(): Observable<WeightEntry[]> {
    return of([...this.weightEntries]);
  }

  get(id: string): Observable<WeightEntry> {
    return of(this.weightEntries.find((we) => we.id === id));
  }

  add(weightEntry: WeightEntry): Promise<WeightEntry> {
    this.weightEntries.push(weightEntry);
    return new Promise<WeightEntry>((resolve) => resolve({ ...weightEntry, id: Guid.create().toString() }));
  }

  update(weightEntry: WeightEntry): Promise<WeightEntry> {
    const existingEntryIndex = this.weightEntries.findIndex((we) => we.id === weightEntry.id);
    if (existingEntryIndex >= 0) {
      const newWeightEntry = { id: weightEntry.id, date: weightEntry.date, weight: weightEntry.weight };
      this.weightEntries[existingEntryIndex] = newWeightEntry;
      return new Promise<WeightEntry>((resolve) => resolve(newWeightEntry));
    } else {
      return new Promise<WeightEntry>((_, reject) => reject());
    }
  }

  remove(weightEntry: WeightEntry): Promise<boolean> {
    const foundWeightEntry = this.weightEntries.find((we) => we.id === weightEntry.id);
    if (foundWeightEntry) {
      this.weightEntries = this.weightEntries.filter((we) => we.id !== weightEntry.id);
    }

    return new Promise<boolean>((resolve) => {
      if (foundWeightEntry) {
        resolve(true);
      } else {
        resolve(false);
      }
    });
  }
}
