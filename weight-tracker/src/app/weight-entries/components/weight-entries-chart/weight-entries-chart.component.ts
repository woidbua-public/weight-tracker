import { Component, ElementRef, Input, OnChanges, OnInit, ViewChild } from '@angular/core';
import {
  CartesianScaleTypeRegistry,
  Chart,
  LinearScale,
  LineController,
  LineElement,
  PointElement,
  ScaleOptionsByType,
  TimeScale,
  TimeUnit,
  Tooltip
} from 'chart.js';
import { _DeepPartialObject } from 'chart.js/types/utils';
import 'chartjs-adapter-moment';
import moment from 'moment';
import { WeightEntry } from '../../models';
import { WeightEntriesSummarizerService } from '../../services/weight-entries-summarizer.service';

moment.locale('de');

Chart.register(TimeScale, LineController, LinearScale, PointElement, LineElement, Tooltip);

enum WeightEntriesChartStyle {
  d7,
  d30,
  m6,
  y1,
  all,
}

@Component({
  selector: 'app-weight-entries-chart',
  templateUrl: './weight-entries-chart.component.html',
  styleUrls: ['./weight-entries-chart.component.scss'],
})
export class WeightEntriesChartComponent implements OnInit, OnChanges {
  @Input() weightEntries: WeightEntry[];
  @ViewChild('weightEntriesChart', { static: true }) private chartRef: ElementRef;

  weightEntryChartStyles = WeightEntriesChartStyle;
  selectedWeightEntryChartStyle: WeightEntriesChartStyle;

  private chart: Chart;

  constructor(private weightEntriesSummarizerService: WeightEntriesSummarizerService) {}

  ngOnInit(): void {
    this.chart = this.generateChart();
    this.set7DayChart();
  }

  ngOnChanges(): void {
    switch (this.selectedWeightEntryChartStyle) {
      case WeightEntriesChartStyle.d7:
        this.set7DayChart();
        break;
      case WeightEntriesChartStyle.d30:
        this.set30DayChart();
        break;
      case WeightEntriesChartStyle.m6:
        this.set6MonthsChart();
        break;
      case WeightEntriesChartStyle.y1:
        this.set1YearChart();
        break;
      case WeightEntriesChartStyle.all:
        this.setAllChart();
        break;
    }
  }

  set7DayChart(): void {
    const endDate = moment().startOf('day').toDate();
    const startDate = moment(endDate).subtract(6, 'days').toDate();

    this.setChartProperties(startDate, endDate, WeightEntriesChartStyle.d7);
  }

  set30DayChart(): void {
    const endDate = moment().startOf('day').toDate();
    const startDate = moment(endDate).subtract(29, 'days').toDate();

    this.setChartProperties(startDate, endDate, WeightEntriesChartStyle.d30);
  }

  set6MonthsChart(): void {
    const endDate = moment().startOf('day').toDate();
    const startDate = moment(endDate).subtract(5, 'months').startOf('month').toDate();

    this.setChartProperties(startDate, endDate, WeightEntriesChartStyle.m6);
  }

  set1YearChart(): void {
    const endDate = moment().startOf('day').toDate();
    const startDate = moment(endDate).subtract(1, 'years').startOf('month').toDate();

    this.setChartProperties(startDate, endDate, WeightEntriesChartStyle.y1);
  }

  setAllChart(): void {
    const endDate = moment().startOf('day').toDate();
    const startDate = this.weightEntries.reduce((pv, cv) => (pv < cv.date ? pv : cv.date), endDate);

    this.setChartProperties(startDate, endDate, WeightEntriesChartStyle.all);
  }

  private setChartProperties(startDate: Date, endDate: Date, weightEntryChartStyle: WeightEntriesChartStyle): void {
    const labels: any[] = [];
    const data: number[] = [];

    const relevantWeightEntries = this.weightEntriesSummarizerService.summarizeWeightEntries(
      this.weightEntries,
      startDate,
      endDate
    );
    relevantWeightEntries.forEach((rwe) => {
      labels.push(moment(rwe.date));
      data.push(rwe.weight);
    });

    const diff = moment(endDate).diff(startDate, 'weeks');

    let chartUnit: TimeUnit;
    if (diff < 1) {
      chartUnit = 'day';
    } else if (diff < 10) {
      chartUnit = 'week';
    } else if (diff < 40) {
      chartUnit = 'month';
    } else if (diff < 120) {
      chartUnit = 'quarter';
    } else {
      chartUnit = 'year';
    }

    this.chart.data.labels = labels;
    this.chart.data.datasets[0].data = data;
    this.chart.options.scales = this.setScales(chartUnit);
    this.selectedWeightEntryChartStyle = weightEntryChartStyle;
    this.chart.update();
  }

  private setScales(
    unit: TimeUnit
  ): _DeepPartialObject<{ [key: string]: ScaleOptionsByType<keyof CartesianScaleTypeRegistry> }> {
    return {
      x: {
        type: 'time',
        time: {
          unit,
          displayFormats: {
            day: 'ddd, DD.MM.YY',
          },
        },
      },
    };
  }

  private generateChart(): Chart {
    return new Chart(this.chartRef.nativeElement, {
      type: 'line',
      data: {
        labels: [],
        datasets: [
          {
            data: [],
            fill: false,
            borderColor: '#50c8ff',
            tension: 0.25,
          },
        ],
      },
      options: {
        scales: {
          x: {
            type: 'time',
            time: {
              unit: 'day',
              displayFormats: {
                day: 'ddd, DD.MM.YY',
              },
            },
          },
        },
        plugins: {
          legend: {
            display: false,
          },
          tooltip: {
            callbacks: {
              title: (context) => moment(context[0].parsed.x).startOf('day').format('ddd, DD MMM YYYY'),
              label: (context) => (Math.round(context.parsed.y * 100) / 100).toString() + ' kg',
            },
          },
        },
      },
    });
  }
}
