import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WeightEntriesPage } from './weight-entries.page';

const routes: Routes = [
  {
    path: '',
    component: WeightEntriesPage,
  },
  {
    path: 'add',
    loadChildren: () =>
      import('./pages/weight-entry-add-edit/weight-entry-add-edit.module').then((m) => m.WeightEntryAddEditPageModule),
  },
  {
    path: 'edit/:id',
    loadChildren: () =>
      import('./pages/weight-entry-add-edit/weight-entry-add-edit.module').then((m) => m.WeightEntryAddEditPageModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WeightEntriesPageRoutingModule {}
