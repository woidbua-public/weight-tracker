import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import firebase from 'firebase/app';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { FirestoreWeighEntry as FirestoreWeightEntry, WeightEntry } from '../../models';
import { WeightEntriesService } from './weight-entries.service';

import Timestamp = firebase.firestore.Timestamp;

@Injectable()
export class FirestoreWeightEntriesService implements WeightEntriesService {
  constructor(private readonly af: AngularFirestore) {}

  getAll(): Observable<WeightEntry[]> {
    return this.getWeightEntryCollection()
      .valueChanges({ idField: 'id' })
      .pipe(
        map((weightEntries) =>
          weightEntries
            .map<WeightEntry>((we) => ({ ...we, date: we.date.toDate() }))
            .sort((a, b) => (a.date > b.date ? -1 : 1))
        )
      );
  }
  get(id: string): Observable<WeightEntry> {
    return this.getWeightEntryDoc(id)
      .valueChanges({ idField: 'id' })
      .pipe(
        map((weightEntry) => ({
          ...weightEntry,
          date: weightEntry.date.toDate(),
        }))
      );
  }
  async add(weightEntry: WeightEntry): Promise<WeightEntry> {
    const firestoreWeightEntryReference = await this.getWeightEntryCollection().add({
      date: Timestamp.fromDate(weightEntry.date),
      weight: weightEntry.weight,
    });

    const firestoreWeightEntrySnapshot = await firestoreWeightEntryReference.get();
    const firestoreWeightEntryData = firestoreWeightEntrySnapshot.data();

    return {
      id: firestoreWeightEntrySnapshot.id,
      date: firestoreWeightEntryData.date.toDate(),
      weight: firestoreWeightEntryData.weight,
    };
  }
  async update(weightEntry: WeightEntry): Promise<WeightEntry> {
    await this.getWeightEntryDoc(weightEntry.id).update({
      date: Timestamp.fromDate(weightEntry.date),
      weight: weightEntry.weight,
    });

    const firestoreWeightEntrySnapshot = await this.getWeightEntryDoc(weightEntry.id).ref.get();
    const firestoreWeightEntryData = firestoreWeightEntrySnapshot.data();

    return {
      id: firestoreWeightEntrySnapshot.id,
      date: firestoreWeightEntryData.date.toDate(),
      weight: firestoreWeightEntryData.weight,
    };
  }

  async remove(weightEntry: WeightEntry): Promise<boolean> {
    await this.getWeightEntryDoc(weightEntry.id).delete();
    return true;
  }

  private getWeightEntryDoc(weightEntryId: string): AngularFirestoreDocument<FirestoreWeightEntry> {
    return this.getWeightEntryCollection().doc(weightEntryId);
  }

  private getWeightEntryCollection(): AngularFirestoreCollection<FirestoreWeightEntry> {
    return this.af.collection<FirestoreWeightEntry>('weight-entries');
  }
}
