import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { WeightEntriesChartComponent, WeightEntryListComponent, WeightEntryListItemComponent } from './components';
import { CollectionEffects } from './effects';
import * as fromWeightEntries from './reducers';
import { WeightEntriesPageRoutingModule } from './weight-entries-routing.module';
import { WeightEntriesPage } from './weight-entries.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    StoreModule.forFeature(fromWeightEntries.featureKey, fromWeightEntries.reducers),
    EffectsModule.forFeature([CollectionEffects]),
    WeightEntriesPageRoutingModule,
  ],
  declarations: [
    WeightEntriesPage,
    WeightEntriesChartComponent,
    WeightEntryListComponent,
    WeightEntryListItemComponent,
  ],
})
export class WeightEntriesPageModule {}
