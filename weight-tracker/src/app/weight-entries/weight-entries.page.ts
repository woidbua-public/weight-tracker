import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { WeightEntriesCollectionPageActions } from './actions';
import { WeightEntry } from './models';
import * as fromWeightEntries from './reducers';

@Component({
  selector: 'app-weight-entries',
  templateUrl: './weight-entries.page.html',
  styleUrls: ['./weight-entries.page.scss'],
})
export class WeightEntriesPage implements OnInit, OnDestroy {
  isLoading$: Observable<boolean>;
  error$: Observable<any>;

  weightEntries: WeightEntry[];

  private weightEntriesSub: Subscription;

  constructor(private weightEntriesStore: Store<fromWeightEntries.WeightEntriesState>) {}

  ngOnInit(): void {
    this.weightEntriesSub = this.weightEntriesStore
      .select(fromWeightEntries.selectAllWeightEntries)
      .subscribe((weightEntries) => {
        this.weightEntries = weightEntries;
      });

    this.isLoading$ = this.weightEntriesStore.select(fromWeightEntries.selectCollectionLoading);
    this.error$ = this.weightEntriesStore.select(fromWeightEntries.selectCollectionError);

    this.weightEntriesStore.dispatch(WeightEntriesCollectionPageActions.enter());
  }

  ngOnDestroy(): void {
    this.weightEntriesSub.unsubscribe();
  }
}
