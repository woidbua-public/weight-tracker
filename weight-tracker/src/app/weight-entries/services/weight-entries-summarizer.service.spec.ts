import { TestBed } from '@angular/core/testing';
import moment from 'moment';
import jsonData from '../fixtures/weight-entries.fixture.json';
import { WeightEntry } from '../models';
import { WeightEntriesSummarizerService } from './weight-entries-summarizer.service';

moment.locale('de');

describe('WeightEntriesSummarizerService', () => {
  const endDate = moment('2020-12-31').startOf('day').toDate();

  let weightEntriesFixture: WeightEntry[];
  let sut: WeightEntriesSummarizerService;

  beforeAll(() => {
    weightEntriesFixture = jsonData
      .map<WeightEntry>((wed) => ({
        id: wed.id,
        date: new Date(wed.date),
        weight: wed.weight,
      }))
      .sort((we1, we2) => (we1.date < we2.date ? 1 : -1));
  });

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WeightEntriesSummarizerService],
    });
    sut = TestBed.inject(WeightEntriesSummarizerService);
  });

  describe('summarizeWeightEntries', () => {
    it('should return all entries when within 7 days.', () => {
      const startDate = moment(endDate).subtract(6, 'days').toDate();
      const expected = weightEntriesFixture.filter((we) => we.date >= startDate && we.date <= endDate);

      const actual = sut.summarizeWeightEntries(weightEntriesFixture, startDate, endDate);

      expect(expected).toHaveSize(7);
      expect(actual).toEqual(expected);
    });

    it('should return all entries when within 30 days.', () => {
      const startDate = moment(endDate).subtract(29, 'days').toDate();
      const expected = weightEntriesFixture.filter((we) => we.date >= startDate && we.date <= endDate);

      const actual = sut.summarizeWeightEntries(weightEntriesFixture, startDate, endDate);

      expect(expected).toHaveSize(30);
      expect(actual).toEqual(expected);
    });

    it('should return week entries when within 31 days.', () => {
      const startDate = moment(endDate).subtract(30, 'days').toDate();
      const expected: WeightEntry[] = [
        { date: moment('2020-11-30').toDate(), weight: 64.764 },
        { date: moment('2020-12-07').toDate(), weight: 66.233 },
        { date: moment('2020-12-14').toDate(), weight: 66.22 },
        { date: moment('2020-12-21').toDate(), weight: 66.249 },
        { date: moment('2020-12-28').toDate(), weight: 65.395 },
      ];

      const actual = sut.summarizeWeightEntries(weightEntriesFixture, startDate, endDate);

      expect(actual).toEqual(expected);
    });

    it('should return month entries when within 211 days.', () => {
      const startDate = moment(endDate).subtract(210, 'days').toDate();
      const expected: WeightEntry[] = [
        { date: moment('2020-06-01').toDate(), weight: 65.72 },
        { date: moment('2020-07-01').toDate(), weight: 65.652 },
        { date: moment('2020-08-01').toDate(), weight: 65.77 },
        { date: moment('2020-09-01').toDate(), weight: 66.584 },
        { date: moment('2020-10-01').toDate(), weight: 64.198 },
        { date: moment('2020-11-01').toDate(), weight: 63.642 },
        { date: moment('2020-12-01').toDate(), weight: 65.841 },
      ];

      const actual = sut.summarizeWeightEntries(weightEntriesFixture, startDate, endDate);

      expect(actual).toEqual(expected);
    });

    it('should return quarter entries when within 901 days.', () => {
      const startDate = moment(endDate).subtract(900, 'days').toDate();
      const expected: WeightEntry[] = [
        { date: moment('2018-07-01').toDate(), weight: 73.533 },
        { date: moment('2018-10-01').toDate(), weight: 74.749 },
        { date: moment('2019-01-01').toDate(), weight: 72.885 },
        { date: moment('2019-04-01').toDate(), weight: 68.166 },
        { date: moment('2019-07-01').toDate(), weight: 66.411 },
        { date: moment('2019-10-01').toDate(), weight: 68.805 },
        { date: moment('2020-01-01').toDate(), weight: 66.535 },
        { date: moment('2020-04-01').toDate(), weight: 64.652 },
        { date: moment('2020-07-01').toDate(), weight: 65.996 },
        { date: moment('2020-10-01').toDate(), weight: 64.571 },
      ];

      const actual = sut.summarizeWeightEntries(weightEntriesFixture, startDate, endDate);

      expect(actual).toEqual(expected);
    });
  });
});
