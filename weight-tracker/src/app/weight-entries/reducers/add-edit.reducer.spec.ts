import moment from 'moment';
import { WeightEntriesAddEditPageActions, WeightEntriesCollectionApiActions } from '../actions';
import { WeightEntry } from '../models';
import * as fromAddEdit from './add-edit.reducer';

moment.locale('de');

describe('WeightEntriesAddEditReducer', () => {
  let NEW_WEIGHT_ENTRY: WeightEntry;
  let EXISTING_WEIGHT_ENTRY: WeightEntry;

  beforeEach(() => {
    NEW_WEIGHT_ENTRY = { date: new Date(2021, 1, 1), weight: 70 };
    EXISTING_WEIGHT_ENTRY = { id: 'existing_id', ...NEW_WEIGHT_ENTRY };
  });

  describe('Actions', () => {
    describe('undefined state', () => {
      it('should return the default state', () => {
        const reducer = fromAddEdit.reducer(undefined, {} as any);

        const expected: fromAddEdit.State = { weightEntry: null, shouldRedirect: false, loading: false, error: null };
        expect(reducer).toEqual(expected);
      });
    });

    describe('enter', () => {
      it('should set correct states', () => {
        const action = () => WeightEntriesAddEditPageActions.enter();

        const result = fromAddEdit.reducer(undefined, action());

        const expected: fromAddEdit.State = {
          weightEntry: { date: moment().startOf('day').toDate(), weight: undefined },
          shouldRedirect: false,
          loading: false,
          error: null,
        };
        expect(result).toEqual(expected);
      });
    });

    describe('load', () => {
      it('should set correct states', () => {
        const action = () => WeightEntriesAddEditPageActions.load({ id: '' });

        const result = fromAddEdit.reducer(undefined, action());

        const expected: fromAddEdit.State = {
          weightEntry: null,
          shouldRedirect: false,
          loading: true,
          error: null,
        };
        expect(result).toEqual(expected);
      });
    });

    describe('loadSuccess', () => {
      it('should set correct states', () => {
        const action = () => WeightEntriesCollectionApiActions.loadSuccess({ weightEntry: EXISTING_WEIGHT_ENTRY });

        const result = fromAddEdit.reducer(undefined, action());

        const expected: fromAddEdit.State = {
          weightEntry: EXISTING_WEIGHT_ENTRY,
          shouldRedirect: false,
          loading: false,
          error: null,
        };
        expect(result).toEqual(expected);
      });
    });

    describe('loadFailure', () => {
      it('should set correct states', () => {
        const error = 'error';
        const action = () => WeightEntriesCollectionApiActions.loadFailure({ error });

        const result = fromAddEdit.reducer(undefined, action());

        const expected: fromAddEdit.State = {
          weightEntry: null,
          shouldRedirect: true,
          loading: false,
          error,
        };
        expect(result).toEqual(expected);
      });
    });

    describe('add', () => {
      it('should set correct states', () => {
        const action = () => WeightEntriesAddEditPageActions.add({ weightEntry: NEW_WEIGHT_ENTRY });

        const result = fromAddEdit.reducer(undefined, action());

        const expected: fromAddEdit.State = {
          weightEntry: null,
          shouldRedirect: false,
          loading: true,
          error: null,
        };
        expect(result).toEqual(expected);
      });
    });

    describe('addSuccess', () => {
      it('should set correct states', () => {
        const action = () => WeightEntriesCollectionApiActions.addSuccess({ weightEntry: EXISTING_WEIGHT_ENTRY });

        const result = fromAddEdit.reducer(undefined, action());

        const expected: fromAddEdit.State = {
          weightEntry: EXISTING_WEIGHT_ENTRY,
          shouldRedirect: true,
          loading: false,
          error: null,
        };
        expect(result).toEqual(expected);
      });
    });

    describe('addFailure', () => {
      it('should set correct states', () => {
        const error = 'error';
        const action = () => WeightEntriesCollectionApiActions.addFailure({ error });

        const result = fromAddEdit.reducer(undefined, action());

        const expected: fromAddEdit.State = {
          weightEntry: null,
          shouldRedirect: false,
          loading: false,
          error,
        };
        expect(result).toEqual(expected);
      });
    });

    describe('update', () => {
      it('should set correct states', () => {
        const action = () => WeightEntriesAddEditPageActions.update({ weightEntry: EXISTING_WEIGHT_ENTRY });

        const result = fromAddEdit.reducer(undefined, action());

        const expected: fromAddEdit.State = {
          weightEntry: null,
          shouldRedirect: false,
          loading: true,
          error: null,
        };
        expect(result).toEqual(expected);
      });
    });

    describe('updateSuccess', () => {
      it('should set correct states', () => {
        const action = () =>
          WeightEntriesCollectionApiActions.updateSuccess({
            weightEntry: EXISTING_WEIGHT_ENTRY,
          });

        const result = fromAddEdit.reducer(undefined, action());

        const expected: fromAddEdit.State = {
          weightEntry: EXISTING_WEIGHT_ENTRY,
          shouldRedirect: true,
          loading: false,
          error: null,
        };
        expect(result).toEqual(expected);
      });
    });

    describe('updateFailure', () => {
      it('should set correct states', () => {
        const error = 'error';
        const action = () => WeightEntriesCollectionApiActions.updateFailure({ error });

        const result = fromAddEdit.reducer(undefined, action());

        const expected: fromAddEdit.State = {
          weightEntry: null,
          shouldRedirect: false,
          loading: false,
          error,
        };
        expect(result).toEqual(expected);
      });
    });

    describe('leave', () => {
      it('should return the default state', () => {
        const action = () => WeightEntriesAddEditPageActions.leave();

        const reducer = fromAddEdit.reducer(undefined, action());

        const expected: fromAddEdit.State = { weightEntry: null, shouldRedirect: false, loading: false, error: null };
        expect(reducer).toEqual(expected);
      });
    });
  });

  describe('Selectors', () => {
    describe('selectWeightEntry', () => {
      it('should return the weight entry', () => {
        const result = fromAddEdit.getWeightEntry({
          weightEntry: EXISTING_WEIGHT_ENTRY,
          shouldRedirect: false,
          loading: false,
          error: null,
        });

        expect(result).toEqual(EXISTING_WEIGHT_ENTRY);
      });
    });

    describe('selectShouldRedirect', () => {
      it('should return the should redirect state', () => {
        const result = fromAddEdit.getShouldRedirect({
          weightEntry: EXISTING_WEIGHT_ENTRY,
          shouldRedirect: false,
          loading: true,
          error: null,
        });

        expect(result).toEqual(false);
      });
    });

    describe('selectLoading', () => {
      it('should return the loading state', () => {
        const result = fromAddEdit.getLoading({
          weightEntry: EXISTING_WEIGHT_ENTRY,
          shouldRedirect: true,
          loading: false,
          error: null,
        });

        expect(result).toEqual(false);
      });
    });

    describe('selectError', () => {
      it('should return the error state', () => {
        const errorMessage = 'error message';
        const result = fromAddEdit.getError({
          weightEntry: EXISTING_WEIGHT_ENTRY,
          shouldRedirect: true,
          loading: false,
          error: errorMessage,
        });

        expect(result).toEqual(errorMessage);
      });
    });
  });
});
