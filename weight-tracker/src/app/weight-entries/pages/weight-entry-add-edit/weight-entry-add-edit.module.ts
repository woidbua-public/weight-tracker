import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { WeightEntryAddEditPageRoutingModule } from './weight-entry-add-edit-routing.module';
import { WeightEntryAddEditPage } from './weight-entry-add-edit.page';

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, ReactiveFormsModule, WeightEntryAddEditPageRoutingModule],
  declarations: [WeightEntryAddEditPage],
})
export class WeightEntryAddEditPageModule {}
