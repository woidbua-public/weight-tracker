import { WeightEntriesCollectionApiActions, WeightEntriesCollectionPageActions } from '../actions';
import { WeightEntry } from '../models';
import * as fromCollection from './collection.reducer';

describe('WeightEntriesCollectionReducer', () => {
  let WEIGHT_ENTRIES: WeightEntry[];

  beforeEach(() => {
    WEIGHT_ENTRIES = [
      { id: '3', date: new Date(2021, 1, 3), weight: 71 },
      { id: '2', date: new Date(2021, 1, 2), weight: 72 },
      { id: '1', date: new Date(2021, 1, 1), weight: 70 },
    ];
  });

  describe('Actions', () => {
    describe('undefined state', () => {
      it('should return the default state', () => {
        const reducer = fromCollection.reducer(undefined, {} as any);

        const expected: fromCollection.State = { ids: [], entities: {}, loading: false, error: null };
        expect(reducer).toEqual(expected);
      });
    });

    describe('enter', () => {
      it('should set loading to true', () => {
        const action = WeightEntriesCollectionPageActions.enter;

        const result = fromCollection.reducer(undefined, action());

        const expected: fromCollection.State = { ids: [], entities: {}, loading: true, error: null };
        expect(result).toEqual(expected);
      });
    });

    describe('loadAllSuccess', () => {
      it('should set entities', () => {
        const action = () => WeightEntriesCollectionApiActions.loadAllSuccess({ weightEntries: WEIGHT_ENTRIES });

        const result = fromCollection.reducer(undefined, action());

        const expected: fromCollection.State = {
          ids: WEIGHT_ENTRIES.map((we) => we.id),
          entities: WEIGHT_ENTRIES.reduce((a, b) => ({ ...a, [b.id]: b }), {}),
          loading: false,
          error: null,
        };
        expect(result).toEqual(expected);
      });
    });

    describe('loadAllFailure', () => {
      it('should set error', () => {
        const error = 'error';
        const action = () => WeightEntriesCollectionApiActions.loadAllFailure({ error });

        const result = fromCollection.reducer(undefined, action());

        const expected: fromCollection.State = { ids: [], entities: {}, loading: false, error };
        expect(result).toEqual(expected);
      });
    });

    describe('addSuccess', () => {
      it('should set entity', () => {
        const action = () => WeightEntriesCollectionApiActions.addSuccess({ weightEntry: WEIGHT_ENTRIES[0] });

        const result = fromCollection.reducer(undefined, action());

        const expected: fromCollection.State = {
          ids: [WEIGHT_ENTRIES[0].id],
          entities: { [WEIGHT_ENTRIES[0].id]: WEIGHT_ENTRIES[0] },
          loading: false,
          error: null,
        };
        expect(result).toEqual(expected);
      });
    });

    describe('updateSuccess', () => {
      it('should update entity', () => {
        const initialState: fromCollection.State = {
          ids: [WEIGHT_ENTRIES[0].id],
          entities: { [WEIGHT_ENTRIES[0].id]: WEIGHT_ENTRIES[0] },
          loading: false,
          error: null,
        };
        const newWeightEntry: WeightEntry = { id: WEIGHT_ENTRIES[0].id, date: new Date(), weight: 100 };
        const action = () =>
          WeightEntriesCollectionApiActions.updateSuccess({
            weightEntry: newWeightEntry,
          });

        const result = fromCollection.reducer(initialState, action());

        const expected: fromCollection.State = {
          ids: [WEIGHT_ENTRIES[0].id],
          entities: { [newWeightEntry.id]: newWeightEntry },
          loading: false,
          error: null,
        };
        expect(result).toEqual(expected);
      });
    });

    describe('remove', () => {
      it('should set loading to true', () => {
        const action = () => WeightEntriesCollectionPageActions.remove({ weightEntry: WEIGHT_ENTRIES[0] });

        const result = fromCollection.reducer(undefined, action());

        const expected: fromCollection.State = { ids: [], entities: {}, loading: true, error: null };
        expect(result).toEqual(expected);
      });
    });

    describe('removeSuccess', () => {
      it('should set correct state', () => {
        const initialState: fromCollection.State = {
          ids: WEIGHT_ENTRIES.map((we) => we.id),
          entities: WEIGHT_ENTRIES.reduce((a, b) => ({ ...a, [b.id]: b }), {}),
          loading: false,
          error: null,
        };
        const action = () => WeightEntriesCollectionApiActions.removeSuccess({ weightEntry: WEIGHT_ENTRIES[1] });

        const result = fromCollection.reducer(initialState, action());

        const expected: fromCollection.State = {
          ids: WEIGHT_ENTRIES.map((we) => we.id).filter((id) => id !== WEIGHT_ENTRIES[1].id),
          entities: { [WEIGHT_ENTRIES[0].id]: WEIGHT_ENTRIES[0], [WEIGHT_ENTRIES[2].id]: WEIGHT_ENTRIES[2] },
          loading: false,
          error: null,
        };
        expect(result).toEqual(expected);
      });
    });

    describe('removeFailure', () => {
      it('should set correct state', () => {
        const error = 'error';
        const action = () => WeightEntriesCollectionApiActions.removeFailure({ error });

        const result = fromCollection.reducer(undefined, action());

        const expected: fromCollection.State = {
          ids: [],
          entities: {},
          loading: false,
          error,
        };
        expect(result).toEqual(expected);
      });
    });
  });

  describe('Selectors', () => {
    describe('selectLoading', () => {
      it('should return the loading state', () => {
        const result = fromCollection.getLoading({
          ids: [],
          entities: {},
          loading: true,
          error: null,
        });

        expect(result).toBeTruthy();
      });
    });

    describe('selectError', () => {
      it('should return the error state', () => {
        const errorMessage = 'error message';
        const result = fromCollection.getError({
          ids: [],
          entities: {},
          loading: true,
          error: errorMessage,
        });

        expect(result).toBe(errorMessage);
      });
    });
  });
});
