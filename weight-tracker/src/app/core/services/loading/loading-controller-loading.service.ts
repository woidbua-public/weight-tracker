import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { LoadingElementOptions, LoadingService } from './loading.service';

@Injectable()
export class LoadingControllerLoadingService extends LoadingService {
  isLoading: boolean;

  constructor(private loadingController: LoadingController) {
    super();
  }

  async present(options: LoadingElementOptions = { message: 'Please wait...' }): Promise<void> {
    this.isLoading = true;

    await this.loadingController.create(options).then((res) => {
      res.present().then(() => {
        if (!this.isLoading) {
          this.dismiss();
        }
      });
    });
  }

  async dismiss(): Promise<void> {
    this.isLoading = false;

    while ((await this.loadingController.getTop()) !== undefined) {
      await this.loadingController.dismiss();
    }
  }
}
