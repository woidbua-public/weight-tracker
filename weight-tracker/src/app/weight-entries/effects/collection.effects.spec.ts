import { TestBed } from '@angular/core/testing';
import { Actions } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { Action } from '@ngrx/store';
import { cold, hot } from 'jasmine-marbles';
import { Observable, of, throwError } from 'rxjs';
import {
  WeightEntriesAddEditPageActions,
  WeightEntriesCollectionApiActions,
  WeightEntriesCollectionPageActions
} from '../actions';
import { WeightEntry } from '../models';
import { WeightEntriesService } from '../services';
import { CollectionEffects } from './collection.effects';

describe('WeightEntriesCollectionEffects', () => {
  let mockWeightEntriesService: jasmine.SpyObj<WeightEntriesService>;
  let effects: CollectionEffects;
  let actions$ = new Observable<Action>();

  let WEIGHT_ENTRIES: WeightEntry[];

  beforeEach(() => {
    WEIGHT_ENTRIES = [
      { id: '1', date: new Date(2021, 1, 1), weight: 70 },
      { id: '2', date: new Date(2021, 1, 2), weight: 72 },
    ];

    mockWeightEntriesService = jasmine.createSpyObj(['getAll', 'get', 'add', 'update', 'remove']);

    TestBed.configureTestingModule({
      providers: [
        CollectionEffects,
        {
          provide: WeightEntriesService,
          useValue: mockWeightEntriesService,
        },
        provideMockActions(() => actions$),
      ],
    });

    effects = TestBed.inject(CollectionEffects);
    actions$ = TestBed.inject(Actions);
  });

  describe('loadCollections$', () => {
    it('should return load success with weight entries', () => {
      const action = WeightEntriesCollectionPageActions.enter;
      const response = cold('-a|', { a: WEIGHT_ENTRIES });
      mockWeightEntriesService.getAll.and.returnValue(response);

      actions$ = hot('-a', { a: action });

      const expectedAction = WeightEntriesCollectionApiActions.loadAllSuccess({ weightEntries: WEIGHT_ENTRIES });
      const expected = cold('--c', { c: expectedAction });
      expect(effects.loadCollection$).toBeObservable(expected);
    });

    it('should return load success with empty entries', () => {
      const action = WeightEntriesCollectionPageActions.enter;
      const response = cold('-a|', { a: [] });
      mockWeightEntriesService.getAll.and.returnValue(response);

      actions$ = hot('-a', { a: action });

      const expectedCompletion = WeightEntriesCollectionApiActions.loadAllSuccess({ weightEntries: [] });
      const expected = cold('--c', { c: expectedCompletion });
      expect(effects.loadCollection$).toBeObservable(expected);
    });

    it('should return load failure when error occurs', () => {
      const action = WeightEntriesCollectionPageActions.enter;
      const response = cold('-#');
      mockWeightEntriesService.getAll.and.returnValue(response);

      actions$ = hot('-a', { a: action });

      const expectedCompletion = WeightEntriesCollectionApiActions.loadAllFailure({ error: 'error' });
      const expected = cold('--c', { c: expectedCompletion });
      expect(effects.loadCollection$).toBeObservable(expected);
    });
  });

  describe('load$', () => {
    it('should return load success with weight entry', (done) => {
      const weightEntry = WEIGHT_ENTRIES[0];
      const response = of(weightEntry);
      mockWeightEntriesService.get.and.returnValue(response);

      actions$ = of(WeightEntriesAddEditPageActions.load({ id: weightEntry.id }));

      const expected = WeightEntriesCollectionApiActions.loadSuccess({ weightEntry });
      effects.load$.subscribe((action) => {
        expect(action).toEqual(expected);
        done();
      });
    });

    it('should return load failure when no weight entry with given id', (done) => {
      const response = of(undefined);
      mockWeightEntriesService.get.and.returnValue(response);

      actions$ = of(WeightEntriesAddEditPageActions.load({ id: 'id' }));

      const expected = WeightEntriesCollectionApiActions.loadFailure({ error: 'No entry found.' });
      effects.load$.subscribe((action) => {
        expect(action).toEqual(expected);
        done();
      });
    });

    it('should return load failure when error occurs', (done) => {
      const error = 'error';
      const response = throwError(error);
      mockWeightEntriesService.get.and.returnValue(response);

      actions$ = of(WeightEntriesAddEditPageActions.load({ id: 'id' }));

      const expected = WeightEntriesCollectionApiActions.loadFailure({ error });
      effects.load$.subscribe((action) => {
        expect(action).toEqual(expected);
        done();
      });
    });
  });

  describe('add$', () => {
    it('should return add success with weight entry', (done) => {
      const weightEntry = WEIGHT_ENTRIES[0];
      const response = new Promise<WeightEntry>((resolve) => resolve(weightEntry));
      mockWeightEntriesService.add.and.returnValue(response);

      actions$ = of(WeightEntriesAddEditPageActions.add({ weightEntry }));

      const expected = WeightEntriesCollectionApiActions.addSuccess({ weightEntry });
      effects.add$.subscribe((action) => {
        expect(action).toEqual(expected);
        done();
      });
    });

    it('should return add failure with error message', (done) => {
      const weightEntry = WEIGHT_ENTRIES[0];
      const error = 'error';
      const response = new Promise<WeightEntry>((_, reject) => reject(error));
      mockWeightEntriesService.add.and.returnValue(response);

      actions$ = of(WeightEntriesAddEditPageActions.add({ weightEntry }));

      const expected = WeightEntriesCollectionApiActions.addFailure({ error });
      effects.add$.subscribe((action) => {
        expect(action).toEqual(expected);
        done();
      });
    });
  });

  describe('update$', () => {
    it('should return update success with weight entry', (done) => {
      const weightEntry = WEIGHT_ENTRIES[0];
      const response = new Promise<WeightEntry>((resolve) => resolve(weightEntry));
      mockWeightEntriesService.update.and.returnValue(response);

      actions$ = of(WeightEntriesAddEditPageActions.update({ weightEntry }));

      const expected = WeightEntriesCollectionApiActions.updateSuccess({
        weightEntry,
      });
      effects.update$.subscribe((action) => {
        expect(action).toEqual(expected);
        done();
      });
    });

    it('should return update failure with error message', (done) => {
      const weightEntry = WEIGHT_ENTRIES[0];
      const error = 'error';
      const response = new Promise<WeightEntry>((_, reject) => reject(error));
      mockWeightEntriesService.update.and.returnValue(response);

      actions$ = of(WeightEntriesAddEditPageActions.update({ weightEntry }));

      const expected = WeightEntriesCollectionApiActions.updateFailure({ error });
      effects.update$.subscribe((action) => {
        expect(action).toEqual(expected);
        done();
      });
    });
  });

  describe('remove$', () => {
    it('should return remove success with weight entry', (done) => {
      const weightEntry = WEIGHT_ENTRIES[0];
      const response = new Promise<boolean>((resolve) => resolve(true));
      mockWeightEntriesService.remove.and.returnValue(response);

      actions$ = of(WeightEntriesCollectionPageActions.remove({ weightEntry }));

      const expected = WeightEntriesCollectionApiActions.removeSuccess({
        weightEntry,
      });
      effects.remove$.subscribe((action) => {
        expect(action).toEqual(expected);
        done();
      });
    });

    it('should return remove failure when id not found', (done) => {
      const weightEntry = WEIGHT_ENTRIES[0];
      const response = new Promise<boolean>((resolve) => resolve(false));
      mockWeightEntriesService.remove.and.returnValue(response);

      actions$ = of(WeightEntriesCollectionPageActions.remove({ weightEntry }));

      const expected = WeightEntriesCollectionApiActions.removeFailure({
        error: 'Weight entry with given id 1 does not exist.',
      });
      effects.remove$.subscribe((action) => {
        expect(action).toEqual(expected);
        done();
      });
    });

    it('should return remove failure with error message', (done) => {
      const weightEntry = WEIGHT_ENTRIES[0];
      const error = 'error';
      const response = new Promise<boolean>((_, reject) => reject(error));
      mockWeightEntriesService.remove.and.returnValue(response);

      actions$ = of(WeightEntriesCollectionPageActions.remove({ weightEntry }));

      const expected = WeightEntriesCollectionApiActions.removeFailure({ error });
      effects.remove$.subscribe((action) => {
        expect(action).toEqual(expected);
        done();
      });
    });
  });
});
