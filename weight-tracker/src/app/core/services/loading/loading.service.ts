export interface LoadingElementOptions {
  message: string;
}

export abstract class LoadingService {
  abstract present(options?: LoadingElementOptions): Promise<void>;

  abstract dismiss(): Promise<void>;
}
