// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firestore: {
    apiKey: 'AIzaSyDfVHJp6XHe82nhmIQ9OWz4jQqVX894fns',
    authDomain: 'woidbua-weighttracker.firebaseapp.com',
    projectId: 'woidbua-weighttracker',
    storageBucket: 'woidbua-weighttracker.appspot.com',
    messagingSenderId: '437525822261',
    appId: '1:437525822261:web:7239997fbbe50e013b2a7c',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
