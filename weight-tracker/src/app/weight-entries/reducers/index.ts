import { Action, combineReducers, createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromRoot from '../../core/reducers';
import * as fromAdd from './add-edit.reducer';
import * as fromCollection from './collection.reducer';

export const featureKey = 'weightEntries';

export interface WeightEntriesState {
  [fromCollection.featureKey]: fromCollection.State;
  [fromAdd.featureKey]: fromAdd.State;
}

export interface State extends fromRoot.State {
  [featureKey]: WeightEntriesState;
}

export const reducers = (state: WeightEntriesState, action: Action): WeightEntriesState =>
  combineReducers({
    [fromCollection.featureKey]: fromCollection.reducer,
    [fromAdd.featureKey]: fromAdd.reducer,
  })(state, action);

export const selectWeightEntriesState = createFeatureSelector<WeightEntriesState>(featureKey);

/**
 * Collection
 */
export const selectCollectionState = createSelector(selectWeightEntriesState, (state) => state.collection);

export const selectCollectionLoading = createSelector(selectCollectionState, fromCollection.getLoading);

export const selectCollectionError = createSelector(selectCollectionState, fromCollection.getError);

export const {
  selectIds: selectCollectionIds,
  selectEntities: selectCollectionEntities,
  selectAll: selectAllWeightEntries,
  selectTotal: selectTotalWeightEntries,
} = fromCollection.adapter.getSelectors(selectCollectionState);

/**
 * Add
 */
export const selectAddEditState = createSelector(selectWeightEntriesState, (state) => state.addEdit);

export const selectAddEditWeightEntry = createSelector(selectAddEditState, fromAdd.getWeightEntry);

export const selectAddEditShouldRedirect = createSelector(selectAddEditState, fromAdd.getShouldRedirect);

export const selectAddEditLoading = createSelector(selectAddEditState, fromAdd.getLoading);

export const selectAddEditError = createSelector(selectAddEditState, fromAdd.getError);
