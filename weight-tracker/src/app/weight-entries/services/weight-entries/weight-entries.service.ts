import { Observable } from 'rxjs';
import { WeightEntry } from '../../models';

export abstract class WeightEntriesService {
  abstract getAll(): Observable<WeightEntry[]>;

  abstract get(id: string): Observable<WeightEntry>;

  abstract add(weightEntry: WeightEntry): Promise<WeightEntry>;

  abstract update(weightEntry: WeightEntry): Promise<WeightEntry>;

  abstract remove(weightEntry: WeightEntry): Promise<boolean>;
}
