import { Action, ActionReducerMap } from '@ngrx/store';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface State {}

export const rootReducers: ActionReducerMap<State, Action> = {};
