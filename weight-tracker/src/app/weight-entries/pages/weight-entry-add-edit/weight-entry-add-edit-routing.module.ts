import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WeightEntryAddEditPage } from './weight-entry-add-edit.page';

const routes: Routes = [
  {
    path: '',
    component: WeightEntryAddEditPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WeightEntryAddEditPageRoutingModule {}
