import { createAction, props } from '@ngrx/store';
import { WeightEntry } from '../models';

const PREFIX = '[Weight Entries | Collection API]';

export const addSuccess = createAction(`${PREFIX} Add Success`, props<{ weightEntry: WeightEntry }>());

export const addFailure = createAction(`${PREFIX} Add Failure`, props<{ error: any }>());

export const updateSuccess = createAction(`${PREFIX} Update Success`, props<{ weightEntry: WeightEntry }>());

export const updateFailure = createAction(`${PREFIX} Update Failure`, props<{ error: any }>());

export const removeSuccess = createAction(`${PREFIX} Remove Success`, props<{ weightEntry: WeightEntry }>());

export const removeFailure = createAction(`${PREFIX} Remove Failure`, props<{ error: any }>());

export const loadAllSuccess = createAction(`${PREFIX} Load All Success`, props<{ weightEntries: WeightEntry[] }>());

export const loadAllFailure = createAction(`${PREFIX} Load All Failure`, props<{ error: any }>());

export const loadSuccess = createAction(`${PREFIX} Load Success`, props<{ weightEntry: WeightEntry }>());

export const loadFailure = createAction(`${PREFIX} Load Failure`, props<{ error: any }>());
