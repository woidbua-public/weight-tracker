import { Component, Input } from '@angular/core';
import { WeightEntry } from '../../models';

@Component({
  selector: 'app-weight-entry-list',
  templateUrl: './weight-entry-list.component.html',
})
export class WeightEntryListComponent {
  @Input() weightEntries: WeightEntry[];
}
