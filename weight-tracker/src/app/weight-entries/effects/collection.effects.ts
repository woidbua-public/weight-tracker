import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { from, of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import {
  WeightEntriesAddEditPageActions,
  WeightEntriesCollectionApiActions,
  WeightEntriesCollectionPageActions
} from '../actions';
import { WeightEntry } from '../models';
import { WeightEntriesService } from '../services';

@Injectable()
export class CollectionEffects {
  loadCollection$ = createEffect(() =>
    this.actions$.pipe(
      ofType(WeightEntriesCollectionPageActions.enter),
      switchMap(() =>
        this.weightEntriesService.getAll().pipe(
          map((weightEntries: WeightEntry[]) => WeightEntriesCollectionApiActions.loadAllSuccess({ weightEntries })),
          catchError((error) => of(WeightEntriesCollectionApiActions.loadAllFailure({ error })))
        )
      )
    )
  );

  load$ = createEffect(() =>
    this.actions$.pipe(
      ofType(WeightEntriesAddEditPageActions.load),
      switchMap(({ id }) =>
        this.weightEntriesService.get(id).pipe(
          map((weightEntry) =>
            weightEntry
              ? WeightEntriesCollectionApiActions.loadSuccess({ weightEntry })
              : WeightEntriesCollectionApiActions.loadFailure({ error: 'No entry found.' })
          ),
          catchError((error) => of(WeightEntriesCollectionApiActions.loadFailure({ error })))
        )
      )
    )
  );

  add$ = createEffect(() =>
    this.actions$.pipe(
      ofType(WeightEntriesAddEditPageActions.add),
      switchMap(({ weightEntry }) =>
        from(this.weightEntriesService.add(weightEntry)).pipe(
          map((result) => WeightEntriesCollectionApiActions.addSuccess({ weightEntry: result })),
          catchError((error) => of(WeightEntriesCollectionApiActions.addFailure({ error })))
        )
      )
    )
  );

  update$ = createEffect(() =>
    this.actions$.pipe(
      ofType(WeightEntriesAddEditPageActions.update),
      switchMap(({ weightEntry }) =>
        from(this.weightEntriesService.update(weightEntry)).pipe(
          map((result) => WeightEntriesCollectionApiActions.updateSuccess({ weightEntry: result })),
          catchError((error) => of(WeightEntriesCollectionApiActions.updateFailure({ error })))
        )
      )
    )
  );

  remove$ = createEffect(() =>
    this.actions$.pipe(
      ofType(WeightEntriesCollectionPageActions.remove),
      switchMap(({ weightEntry }) =>
        from(this.weightEntriesService.remove(weightEntry)).pipe(
          map((result) =>
            result
              ? WeightEntriesCollectionApiActions.removeSuccess({ weightEntry })
              : WeightEntriesCollectionApiActions.removeFailure({
                  error: `Weight entry with given id ${weightEntry.id} does not exist.`,
                })
          ),
          catchError((error) => of(WeightEntriesCollectionApiActions.removeFailure({ error })))
        )
      )
    )
  );

  constructor(private actions$: Actions, private weightEntriesService: WeightEntriesService) {}
}
