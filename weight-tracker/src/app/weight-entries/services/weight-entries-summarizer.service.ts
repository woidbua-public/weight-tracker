import { Injectable } from '@angular/core';
import moment from 'moment';
import { WeightEntry } from '../models';

moment.locale('de');

@Injectable({
  providedIn: 'root',
})
export class WeightEntriesSummarizerService {
  summarizeWeightEntries(weightEntries: WeightEntry[], startDate: Date, endDate: Date): WeightEntry[] {
    const diff = moment(endDate).diff(startDate, 'days');

    if (diff <= 30 - 1) {
      // 30 day entries
      return weightEntries.filter((we) => we.date >= startDate && we.date <= endDate);
    } else if (diff <= 7 * 30 - 1) {
      // 30 week entries
      return this.generateWeekWeightEntries(weightEntries, startDate, endDate);
    } else if (diff <= 30 * 30 - 1) {
      // 30 month entries
      return this.generateMonthWeightEntries(weightEntries, startDate, endDate);
    } else {
      return this.generateQuarterWeightEntries(weightEntries, startDate, endDate);
    }
  }

  private generateWeekWeightEntries(weightEntries: WeightEntry[], startDate: Date, endDate: Date): WeightEntry[] {
    return this.generateSummarizedWeightEntries(weightEntries, startDate, endDate, 'week', 'weeks');
  }

  private generateMonthWeightEntries(weightEntries: WeightEntry[], startDate: Date, endDate: Date): WeightEntry[] {
    return this.generateSummarizedWeightEntries(weightEntries, startDate, endDate, 'month', 'months');
  }

  private generateQuarterWeightEntries(weightEntries: WeightEntry[], startDate: Date, endDate: Date): WeightEntry[] {
    return this.generateSummarizedWeightEntries(weightEntries, startDate, endDate, 'quarter', 'quarters');
  }

  private generateSummarizedWeightEntries(
    weightEntries: WeightEntry[],
    startDate: Date,
    endDate: Date,
    startOfUnit: moment.unitOfTime.StartOf,
    durationConstructor: moment.unitOfTime.DurationConstructor
  ): WeightEntry[] {
    const summarizedWeightEntries: WeightEntry[] = [];

    let currentStartDate = this.getMaxDate(moment(startDate).startOf(startOfUnit).startOf('day').toDate(), startDate);
    let currentEndDate = this.getMinDate(moment(currentStartDate).endOf(startOfUnit).startOf('day').toDate(), endDate);

    while (currentStartDate <= endDate) {
      const relevantWeightEntries = weightEntries.filter(
        (we) => we.date >= currentStartDate && we.date <= currentEndDate
      );

      if (relevantWeightEntries.length > 0) {
        summarizedWeightEntries.push({
          date: moment(currentStartDate).startOf(startOfUnit).startOf('day').toDate(),
          weight:
            Math.round(
              (relevantWeightEntries.reduce((pv, cv) => pv + cv.weight, 0) / relevantWeightEntries.length) * 1000
            ) / 1000,
        });
      }

      currentStartDate = moment(currentStartDate).add(1, durationConstructor).startOf(startOfUnit).toDate();
      currentEndDate = this.getMinDate(moment(currentStartDate).endOf(startOfUnit).toDate(), endDate);
    }

    return summarizedWeightEntries;
  }

  private getMinDate(date1: Date, date2: Date): Date {
    return date1 < date2 ? date1 : date2;
  }

  private getMaxDate(date1: Date, date2: Date): Date {
    return date1 > date2 ? date1 : date2;
  }
}
