import firebase from 'firebase/app';

import Timestamp = firebase.firestore.Timestamp;

export interface WeightEntry {
  id?: string;
  date: Date;
  weight: number;
}

export interface FirestoreWeighEntry {
  date: Timestamp;
  weight: number;
}
