import { createAction, props } from '@ngrx/store';
import { WeightEntry } from '../models';

const PREFIX = '[Weight Entries | Add Edit Page]';

export const enter = createAction(`${PREFIX} Enter`);

export const load = createAction(`${PREFIX} Load`, props<{ id: string }>());

export const add = createAction(`${PREFIX} Add`, props<{ weightEntry: WeightEntry }>());

export const update = createAction(`${PREFIX} Update`, props<{ weightEntry: WeightEntry }>());

export const leave = createAction(`${PREFIX} Leave`);
