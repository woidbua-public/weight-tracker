import { createReducer, on } from '@ngrx/store';
import moment from 'moment';
import { WeightEntriesAddEditPageActions, WeightEntriesCollectionApiActions } from '../actions';
import { WeightEntry } from '../models';

moment.locale('de');

export const featureKey = 'addEdit';

export interface State {
  weightEntry: WeightEntry;
  shouldRedirect: boolean;
  loading: boolean;
  error: any;
}

export const initialState: State = {
  weightEntry: null,
  shouldRedirect: false,
  loading: false,
  error: null,
};

const setNewWeightEntry = (): WeightEntry => ({ date: moment().startOf('day').toDate(), weight: undefined });

export const reducer = createReducer(
  initialState,
  on(WeightEntriesAddEditPageActions.enter, (state) => ({
    ...state,
    weightEntry: setNewWeightEntry(),
    shouldRedirect: false,
    loading: false,
    error: null,
  })),
  on(WeightEntriesAddEditPageActions.load, (state) => ({
    ...state,
    weightEntry: null,
    shouldRedirect: false,
    loading: true,
    error: null,
  })),
  on(WeightEntriesCollectionApiActions.loadSuccess, (state, { weightEntry }) => ({
    ...state,
    weightEntry,
    shouldRedirect: false,
    loading: false,
    error: null,
  })),
  on(WeightEntriesCollectionApiActions.loadFailure, (state, { error }) => ({
    ...state,
    shouldRedirect: true,
    loading: false,
    error,
  })),
  on(WeightEntriesAddEditPageActions.add, (state) => ({ ...state, loading: true })),
  on(WeightEntriesCollectionApiActions.addSuccess, (state, { weightEntry }) => ({
    ...state,
    weightEntry,
    shouldRedirect: true,
    loading: false,
    error: null,
  })),
  on(WeightEntriesCollectionApiActions.addFailure, (state, { error }) => ({
    ...state,
    shouldRedirect: false,
    loading: false,
    error,
  })),
  on(WeightEntriesAddEditPageActions.update, (state) => ({ ...state, loading: true })),
  on(WeightEntriesCollectionApiActions.updateSuccess, (state, { weightEntry }) => ({
    ...state,
    weightEntry,
    shouldRedirect: true,
    loading: false,
    error: null,
  })),
  on(WeightEntriesCollectionApiActions.updateFailure, (state, { error }) => ({
    ...state,
    shouldRedirect: false,
    loading: false,
    error,
  })),
  on(WeightEntriesAddEditPageActions.leave, () => ({ ...initialState }))
);

export const getWeightEntry = (state: State): WeightEntry => state.weightEntry;

export const getShouldRedirect = (state: State): boolean => state.shouldRedirect;

export const getLoading = (state: State): boolean => state.loading;

export const getError = (state: State): any => state.error;
