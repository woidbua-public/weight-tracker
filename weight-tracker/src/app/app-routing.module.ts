import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'weight-entries',
    pathMatch: 'full',
  },
  {
    path: 'weight-entries',
    loadChildren: () => import('./weight-entries/weight-entries.module').then((m) => m.WeightEntriesPageModule),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
