import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import { WeightEntriesCollectionApiActions, WeightEntriesCollectionPageActions } from '../actions';
import { WeightEntry } from '../models';

export const featureKey = 'collection';

export interface State extends EntityState<WeightEntry> {
  loading: boolean;
  error: any;
}

export const adapter: EntityAdapter<WeightEntry> = createEntityAdapter<WeightEntry>({
  selectId: (weightEntry: WeightEntry) => weightEntry.id,
  sortComparer: (we1: WeightEntry, we2: WeightEntry) => (we1.date < we2.date ? 1 : -1),
});

export const initialState: State = adapter.getInitialState({
  loading: false,
  error: null,
});

export const reducer = createReducer(
  initialState,
  on(WeightEntriesCollectionPageActions.enter, (state) => ({ ...state, loading: true, error: null })),
  on(WeightEntriesCollectionApiActions.loadAllSuccess, (state, { weightEntries }) =>
    adapter.setAll(weightEntries, {
      ...state,
      loading: false,
      error: null,
    })
  ),
  on(WeightEntriesCollectionApiActions.loadAllFailure, (state, { error }) => ({ ...state, loading: false, error })),
  on(WeightEntriesCollectionApiActions.addSuccess, (state, { weightEntry }) => adapter.addOne(weightEntry, state)),
  on(WeightEntriesCollectionApiActions.updateSuccess, (state, { weightEntry }) =>
    adapter.upsertOne(weightEntry, state)
  ),
  on(WeightEntriesCollectionPageActions.remove, (state) => ({ ...state, loading: true })),
  on(WeightEntriesCollectionApiActions.removeSuccess, (state, { weightEntry }) =>
    adapter.removeOne(weightEntry.id, { ...state, loading: false, error: null })
  ),
  on(WeightEntriesCollectionApiActions.removeFailure, (state, { error }) => ({ ...state, loading: false, error }))
);

export const getLoading = (state: State): boolean => state.loading;

export const getError = (state: State): any => state.error;
