import { Component, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Store } from '@ngrx/store';
import moment from 'moment';
import { Observable, Subscription } from 'rxjs';
import { WeightEntriesAddEditPageActions } from '../../actions';
import { WeightEntry } from '../../models';
import * as fromWeightEntries from '../../reducers';

moment.locale('de');

@Component({
  selector: 'app-weight-entry-add-edit',
  templateUrl: './weight-entry-add-edit.page.html',
  styleUrls: ['./weight-entry-add-edit.page.scss'],
})
export class WeightEntryAddEditPage implements OnInit, OnDestroy {
  form: FormGroup;

  isLoading$: Observable<boolean>;

  private paramMapSub: Subscription;
  private weightEntrySub: Subscription;
  private shouldRedirectSub: Subscription;

  private weightEntryId?: string;

  constructor(
    private route: ActivatedRoute,
    private weightEntriesStore: Store<fromWeightEntries.WeightEntriesState>,
    private navController: NavController
  ) {}

  ngOnInit(): void {
    this.form = new FormGroup({
      weight: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required, Validators.min(0)],
      }),
      date: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required],
      }),
    });

    this.paramMapSub = this.route.paramMap.subscribe((paramMap) => {
      if (paramMap.has('id')) {
        this.weightEntriesStore.dispatch(WeightEntriesAddEditPageActions.load({ id: paramMap.get('id') }));
      } else {
        this.weightEntriesStore.dispatch(WeightEntriesAddEditPageActions.enter());
      }
    });

    this.weightEntrySub = this.weightEntriesStore
      .select(fromWeightEntries.selectAddEditWeightEntry)
      .subscribe((weightEntry) => {
        if (!weightEntry) {
          return;
        }

        this.weightEntryId = weightEntry.id;
        const weight = weightEntry.weight ? Math.round(weightEntry.weight * 100) / 100 : undefined;
        this.setWeight(weight);
        this.setDate(weightEntry.date);
      });

    this.shouldRedirectSub = this.weightEntriesStore
      .select(fromWeightEntries.selectAddEditShouldRedirect)
      .subscribe((shouldRedirect) => {
        if (shouldRedirect) {
          this.navController.navigateBack(['/']);
        }
      });

    this.isLoading$ = this.weightEntriesStore.select(fromWeightEntries.selectAddEditLoading);
  }

  get weightControl(): AbstractControl {
    return this.form.get('weight');
  }

  get dateControl(): AbstractControl {
    return this.form.get('date');
  }

  saveOrUpdate(): void {
    if (this.form.invalid) {
      return;
    }

    const weightEntry: WeightEntry = {
      id: this.weightEntryId,
      date: moment(this.dateControl.value).startOf('day').toDate(),
      weight: +this.weightControl.value,
    };

    if (weightEntry.id) {
      this.weightEntriesStore.dispatch(
        WeightEntriesAddEditPageActions.update({
          weightEntry,
        })
      );
    } else {
      this.weightEntriesStore.dispatch(WeightEntriesAddEditPageActions.add({ weightEntry }));
    }
  }

  ngOnDestroy(): void {
    this.weightEntriesStore.dispatch(WeightEntriesAddEditPageActions.leave());

    this.paramMapSub.unsubscribe();
    this.weightEntrySub.unsubscribe();
    this.shouldRedirectSub.unsubscribe();
  }

  private setWeight(weight: number) {
    this.form.controls.weight.setValue(weight);
  }

  private setDate(date: Date) {
    this.form.controls.date.setValue(date.toISOString());
  }
}
