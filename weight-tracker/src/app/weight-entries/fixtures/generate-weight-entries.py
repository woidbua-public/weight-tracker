import datetime
import json
import pathlib
import random
import uuid

parent_path = pathlib.Path(__file__).parent.absolute().as_posix()

date_start = datetime.datetime(2015, 1, 1).date()
date_end = datetime.datetime(2022, 1, 1).date()
step = datetime.timedelta(days=1)

start_weight = 75

data = []

date_current = date_start
while date_current <= date_end:
  data.append({
    "id": str(uuid.uuid4()),
    "date": date_current.strftime('%Y-%m-%dT%H:%M:%S'),
    "weight": start_weight
  })
  date_current += step
  start_weight += random.random() - 0.5

with open(parent_path + '/weight-entries.fixture.json', 'w') as outfile:
  json.dump(data, outfile, sort_keys=False, indent=4)
