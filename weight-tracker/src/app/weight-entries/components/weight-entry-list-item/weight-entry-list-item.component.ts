import { Component, Input } from '@angular/core';
import { AlertController, IonItemSliding, NavController } from '@ionic/angular';
import { Store } from '@ngrx/store';
import { WeightEntriesCollectionPageActions } from '../../actions';
import { WeightEntry } from '../../models';
import * as fromWeightEntries from '../../reducers';

@Component({
  selector: 'app-weight-entry-list-item',
  templateUrl: './weight-entry-list-item.component.html',
})
export class WeightEntryListItemComponent {
  @Input() weightEntry: WeightEntry;

  constructor(
    private alertController: AlertController,
    private weightEntriesStore: Store<fromWeightEntries.WeightEntriesState>,
    private navController: NavController
  ) {}

  onEdit(slidingElement: IonItemSliding): void {
    slidingElement.close();
    this.navController.navigateForward(['/', 'weight-entries', 'edit', this.weightEntry.id]);
  }

  onDelete(slidingElement: IonItemSliding): void {
    this.alertController
      .create({
        message: 'Do you really want to delete the weight entry?',
        buttons: [
          { text: 'Delete', handler: () => this.delete(slidingElement) },
          { text: 'Cancel', role: 'cancel' },
        ],
      })
      .then((alertElement) => {
        alertElement.present();
      });
  }

  private delete(slidingElement: IonItemSliding): void {
    slidingElement.close();
    this.weightEntriesStore.dispatch(WeightEntriesCollectionPageActions.remove({ weightEntry: this.weightEntry }));
  }
}
